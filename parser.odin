package main

import "core:fmt"
import "core:time"

Parser :: struct {
    tokens: [dynamic]Token,
    values: map[string]Value,
    open_brackets: int,
    open_braces: int,
    index: int,
}

step_token :: proc(using parser: ^Parser) {
    index += 1;
}

peek_token :: proc(using parser: ^Parser) -> Token {
    return tokens[index+1];
}

prev_token :: proc(using parser: ^Parser) -> Token {
    return tokens[index-1];
}

parse_value :: proc(using parser: ^Parser, array: bool, m: bool) -> (Value, Result) {
        fmt.println(index);
    value_found: bool;
    temp_arr: [dynamic]Value;
    temp_map: map[string]Value;
    if array {
        temp_arr = make([dynamic]Value);
    }
    if m   {
        temp_map = make(map[string]Value);
    }
    token := tokens[index];
    for {
        fmt.println(index);
        switch token.type {
            case .comment:
                continue;
            case .ident:
            case .literal_string:
                if array {
                    append(&temp_arr, token.content.(string));
                } else {
                    return token.content.(string), Nothing{};
                }
            case .literal_float:
                if array {
                    append(&temp_arr, token.content.(f64));
                } else {
                    return token.content.(f64), Nothing{};
                }
            case .literal_int:
                if array {
                    append(&temp_arr, token.content.(i64));
                } else {
                    return token.content.(i64), Nothing{};
                }
            case .equals:
                if m {
                    prev := prev_token(parser);
                    if prev.type != .ident {
                        return 0, ParserError { "Key in inline table is not ident", Pos { prev.l0, prev.c0 }, Pos { prev.l1, prev.c1 } };
                    }
                    fmt.println("this is", prev.content.(string));
                    index += 1;
                    value, res := parse_value(parser, false, false);
                    if res_, ok := res.(Nothing); ok {
                        temp_map[prev.content.(string)] = value;
                    } else {
                        return value, res;
                    }
                }
            case .bracket_open:
                open_brackets += 1;
                index += 1;
                parse_value(parser, true, false);
            case .bracket_close:
                open_brackets -= 1;
            case .brace_open:
                open_braces += 1;
                index += 1;
                parse_value(parser, false, true);
            case .brace_close:
                open_braces -= 1;
                return temp_arr, Nothing{};
            case .newline:
            case .comma:
            case .dot:
        }
        index += 1;
        token = tokens[index];
    }
}

parser :: proc(input: string) -> (map[string]Value, Result) {
    when ODIN_DEBUG { cur := time.tick_now(); }
    parser := Parser{};
    temp_tokens, error := lexer(input);
    defer delete(temp_tokens);
    using parser;
    tokens = temp_tokens;
    values = make(map[string]Value);
    switch err in error {
        case Nothing:
        case ParserError:
            when ODIN_DEBUG {fmt.printf("%v from line %v, char %v", err.errmsg, err.pos0.line, err.pos0.char);}
            return values, err;
    }
    for token in tokens{
        fmt.println(token);
        /*
            switch token.type {
                case .ident:
                    fmt.printf("%v", token.content);
                case .comment:
                    fmt.println(token.content);
                case .literal_string:
                    fmt.printf("\"%v\"", token.content);
                case .bracket_open:
                    fmt.print("[");
                case .bracket_close:
                    fmt.print("]");
                case .brace_open:
                    fmt.print("{");
                case .brace_close:
                    fmt.print("}");
                case .equals:
                    fmt.print("=");
                case .comma:
                    fmt.print(",");
                case .dot:
                    fmt.print(".");
                case .literal_int:
                    fmt.printf("%v", token.content);
                case .literal_float:
                    fmt.printf("%v", token.content);
                case .newline:
                    fmt.println();

            }
            */
    }
    token: Token;
    for len(tokens) > index {
        token = tokens[index];
        fmt.println(token);
        switch token.type {
            case .comment:
                continue;
            case .ident:
            case .literal_string:
            case .literal_float:
            case .literal_int:
            case .equals:
                key_token := tokens[index-1];
                key, ok := key_token.content.(string);
                if !ok {
                    return values, ParserError { "token is not string", Pos { key_token.l0, key_token.c0 }, Pos { key_token.l1, key_token.c1 } };
                }
                fmt.println(key);
                fmt.println(index);
                value, res := parse_value(&parser, false, false);
                fmt.println(index);
                values[key] = value;
            case .bracket_open:
                open_brackets += 1;
            case .bracket_close:
                open_brackets -= 1;
            case .brace_open:
                open_braces += 1;
            case .brace_close:
                open_braces -= 1;
            case .newline:
            case .comma:
            case .dot:
        }
        index += 1;
    }
    when ODIN_DEBUG { fmt.println("parser time:", time.tick_since(cur)); }
    return values, Nothing{};
}
