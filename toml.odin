package main

import "core:time"
import "core:fmt"
import "core:strings"
import "core:strconv"
import "core:unicode"

Value :: union {
    [dynamic]Value,
    bool,
    f64,
    i64,
    string,
    map[string]Value,
}

TokenValue :: union {
    i64, f64, string,
}

StrType :: enum {
    none,
    str,
    literal,
    multiline,
    multiline_literal,
    num,
    ident,
    comment,
    unicode,
}

Nothing :: struct {}

Pos :: struct {
    line: i32,
    char: i32,
}

ParserError :: struct {
    errmsg: string,
    pos0: Pos,
    pos1: Pos,
}

Result :: union {
    Nothing,
    ParserError,
}

Token :: struct {
    l0, l1, c0, c1: i32,
    type: TokenType,
    content: TokenValue,
}

TokenType :: enum {
    comment,
    ident,
    literal_string,
    literal_float,
    literal_int,
    equals,
    bracket_open,
    bracket_close,
    brace_open,
    brace_close,
    newline,
    comma,
    dot,
}

destroy_value :: proc(value: Value) {
    switch v in value {
        case bool, i64, f64:
        case string:
            delete(v);
        case [dynamic]Value:
            for val in v {
                destroy_value(val);
            }
            delete(v);
        case map[string]Value:
            for key, val in v {
                delete(key);
                destroy_value(val);
            }
            delete(v);
    }
}

//destroy_tokens :: proc(tokens: [dynamic]Token) {
    //for token in tokens {
        //if token.type == .comment || token.type == .ident || token.type == .literal_string {
            //delete(token.content.(string));
        //}
    //}
    //delete(tokens);
//}

Lexer :: struct {
    escape, l0, l1, c0, c1: i32,
    long: bool,
    escaped: bool,
    str: StrType,
    seen_dot: bool,
    content: strings.Builder,
    tokens: [dynamic]Token,
}

handle_chars :: proc(using lexer: ^Lexer, char: rune, index: int, input: string) -> bool {
    c1 += 1;
    switch char {
        case '\n':
            if len(tokens) > 0 && tokens[len(tokens)-1].type != .newline && tokens[len(tokens)-1].type != .comma {
                append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.newline,content=""});
                strings.reset_builder(&content);
            }
            return true;
        case ' ':
            return true;
        case '#':
            str = .comment;
        case '"':
            if len(input) > index + 2 {
                if input[index:index+3] == "\"\"\"" {
                    str = .multiline;
                } else {
                    str = .str;
                }
            } else {
                str = .str;
            }
            strings.reset_builder(&content);
            return true;
        case '\'':
            if len(input) > index + 2 {
                if input[index:index+3] == "'''" {
                    str = .multiline_literal;
                } else {
                    str = .literal;
                }
            } else {
                str = .literal;
            }
            strings.reset_builder(&content);
            return true;
        case '=':
            append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.equals,content=""});
            strings.reset_builder(&content);
            return true;
        case ',':
            append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.comma,content=""});
            strings.reset_builder(&content);
            return true;
        case '{':
            append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.brace_open,content=""});
            strings.reset_builder(&content);
            return true;
        case '}':
            append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.brace_close,content=""});
            strings.reset_builder(&content);
            return true;
        case '[':
            append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.bracket_open,content=""});
            strings.reset_builder(&content);
            return true;
        case ']':
            append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.bracket_close,content=""});
            strings.reset_builder(&content);
            return true;
        case '.':
            append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.dot,content=""});
            strings.reset_builder(&content);
            return true;
        case:
    }
    return false;
}

lexer :: proc(input: string) -> ([dynamic]Token, Result) {
    cur := time.tick_now();
    lexer := Lexer{};
    using lexer;
    str = StrType.none;
    content = strings.make_builder();
    defer strings.destroy_builder(&content);
    tokens = make([dynamic]Token);
    has := -2;
    for char, index in input {
        if char == '\n' {
            c1 = 0;
            l1 += 1;
        }
        //fmt.println(strings.to_string(content));
        switch str {
            case .none:
                if len(tokens) > 0 && tokens[len(tokens)-1].type != .newline && unicode.is_digit(char) {
                    str = .num;
                } else if unicode.is_letter(char) || unicode.is_digit(char) || char == '_' || char == '-' {
                    str = .ident;
                } else {
                    if handle_chars(&lexer, char, index, input) {
                        continue;
                    }
                }
            case .str:
                if !escaped {
                    if char == '\\' {
                        escaped = true;
                    }
                    if char == '\n' {
                        return tokens, ParserError { "Missing \"", Pos { l0, c0 }, Pos { l1, c1 } };
                    }
                    if char == '"' {
                        append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.literal_string,content=strings.clone(strings.to_string(content))});
                        l0=l1;
                        c0=c1;
                        strings.reset_builder(&content);
                        str = .none;
                        continue;
                    }
                } else {
                    switch char {
                        case '"':
                            strings.write_rune_builder(&content, '"');
                        case 'n':
                            strings.write_rune_builder(&content, '\n');
                        case 'b':
                            strings.write_rune_builder(&content, 0x005c);
                        case 'r':
                            strings.write_rune_builder(&content, 0x000d);
                        case 't':
                            strings.write_rune_builder(&content, '\t');
                        case '\\':
                            strings.write_rune_builder(&content, '\\');
                        case 'u':
                            escape = 4;
                            long = false;
                            //str = .unicode;
                        case 'U':
                            escape = 8;
                            long = true;
                            //str = .unicode;
                    }
                    continue;
                }
            case .literal:
                    if char == '\n' {
                        return tokens, ParserError { "Missing '", Pos { l0, c0 }, Pos { l1, c1 } };
                    }
                    if char == '\'' {
                        append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.literal_string,content=strings.clone(strings.to_string(content))});
                        l0=l1;
                        c0=c1;
                        strings.reset_builder(&content);
                        str = .none;
                        continue;
                    }
            case .multiline:
                if char == '\n' {
                    if len(strings.to_string(content)) == 0 {
                        continue;
                    }
                }
                if has == 2{
                    if char == '"' {
                        append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.literal_string,content=strings.clone(strings.to_string(content))});
                        l0=l1;
                        c0=c1;
                        strings.reset_builder(&content);
                        str = .none;
                        has = -2;
                        continue;
                    } else {
                        strings.write_string(&content, "\"\"");
                        has = 0;
                    }
                } else {
                    if char == '"' {
                        has += 1;
                        continue;
                    }
                }
            case .multiline_literal:
                if char == '\n' {
                    if len(strings.to_string(content)) == 0 {
                        continue;
                    }
                }
                if has == 2 {
                    if char == '\'' {
                        append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.literal_string,content=strings.clone(strings.to_string(content))});
                        l0=l1;
                        c0=c1;
                        strings.reset_builder(&content);
                        str = .none;
                        has = -2;
                        continue;
                    } else {
                        strings.write_string(&content, "''");
                        has = 0;
                    }
                } else {
                    if char == '\'' {
                        has += 1;
                        continue;
                    }
                }
            case .ident:
                if !(unicode.is_letter(char) || unicode.is_digit(char) || char == '_' || char == '-') {
                    append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.ident,content=strings.clone(strings.to_string(content))});
                    l0=l1;
                    c0=c1;
                    strings.reset_builder(&content);
                    str = .none;
                    if handle_chars(&lexer, char, index, input) {
                        continue;
                    }
                    continue;
                }
            case .num:
                if char == '.' {
                    if !seen_dot {
                        seen_dot = true;
                    } else {
                        return tokens, ParserError { "That is not a valid float tho!", Pos { l0, c0 }, Pos { l1, c1 } };
                    }
                }
                if !unicode.is_digit(char) && char != '.' {
                    if seen_dot {
                        append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.literal_int,content=strconv.atof(strings.to_string(content))});
                        seen_dot = false;
                    } else {
                        append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.literal_int,content=cast(i64)strconv.atoi(strings.to_string(content))});
                    }
                    l0=l1;
                    c0=c1;
                    strings.reset_builder(&content);
                    str = .none;
                    if handle_chars(&lexer, char, index, input) {
                        continue;
                    }
                    continue;
                }
            case .comment:
                if char == '\n' {
                    // discard comments, who needs them anyways
                    //append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.comment,content=strings.clone(strings.to_string(content))});
                    append(&tokens, Token{l0=l0,l1=l1,c0=c0,c1=c1,type=.newline,content=""});
                    l0=l1;
                    c0=c1;
                    strings.reset_builder(&content);
                    str = .none;
                    continue;
                }
            case .unicode:
                escape -= 1;
                strings.write_rune_builder(&content, char);
                if escape <= 0 {
                    strn := strings.to_string(content);
                    if long {
                        fmt.tprintf("0x%v", strn[len(strn)-8:]);
                    } else {
                        fmt.tprintf("0x%v", strn[len(strn)-4:]);
                    }
                    str = .str;
                }
                continue;
            case:
        }
        l0 = l1;
        c0 = c1;
        strings.write_rune_builder(&content, char);
    }
    fmt.println(time.tick_since(cur));
    //delete_all(context.temp_allocator);
    return tokens, Nothing{};
}


main :: proc() {
    values, error := parser(`
"be=es" = "r= [  u eeeeee" # ene = ""
cat = ["bees",
    "bee",
    0124,
[[["ee"],[["bee"]]]]

    ]

eintrs = 'oiaresnt"n orasti()&%()_@(\\n\\n\\n'

sroietg = """
ri\' ""sonta
beesion
"""

oiaerstm = '''
oyuarntft'''

[ynuaarign]
bee = true # oiaersnntoieantn

coal = 24561

brr.no = 2.880

218 = "\\u0061ees"

[eionret]
skrrt = 'brrr'
point = { x = 1, y = 2}
nested = { nested = { nested = { nested = 'bees' } } }`);
    //tokens, error := lexer(`"""bees"""`);
    //defer destroy_value(values);
        for key, value in values {
            /*switch token.type {
                case .ident:
                    fmt.printf("%v", token.content);
                case .comment:
                    fmt.println(token.content);
                case .literal_string:
                    fmt.printf("\"%v\"", token.content);
                case .bracket_open:
                    fmt.print("[");
                case .bracket_close:
                    fmt.print("]");
                case .brace_open:
                    fmt.print("{");
                case .brace_close:
                    fmt.print("}");
                case .equals:
                    fmt.print("=");
                case .comma:
                    fmt.print(",");
                case .dot:
                    fmt.print(".");
                case .literal_int:
                case .literal_float:
                case .newline:
                    fmt.println();
            }
            */
            fmt.printf("%v: %v\n", key, value);
        }
}
